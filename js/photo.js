var p_model = {
    piclst: undefined,

    init: function(callback) {
        try {
            _500px.init({
                sdk_key: 'e476f5b9eff64f28ed8cb61e8c182b0267dd752e'
            });
        } catch (error) {
            console.log(error);
        } finally {
            if (!localStorage.pics) {
                _500px.api('/photos', { feature: 'popular', page: 1, image_size: [2, 4] }, function(response) {
                    console.log(response.data);
                    localStorage.pics = JSON.stringify({
                        pic_array: response.data.photos,
                        pic_page: 1,
                        pic_index: 0,
                        pic_bookmark: []
                    });
                    this.piclst = JSON.parse(localStorage.pics);
                    this.piclst.pic_bookmark = new Set(this.piclst.pic_bookmark);
                    callback(0);
                }.bind(this));
            } else {
                this.piclst = JSON.parse(localStorage.pics);
                this.piclst.pic_bookmark = new Set(this.piclst.pic_bookmark);
                console.log(this.piclst.pic_index);
                callback(this.piclst.pic_index);
            }
        }
    },

    get_pics: function(pos, callback) {
        var l = this.piclst.pic_array.length;
        if (pos + 9 > l - 1) {
            this.loadmore(pos, callback);
            return undefined;
        }
        return this.piclst.pic_array.slice(pos, pos + 10);
    },

    loadmore: function(pos, callback) {
        var pg = this.piclst.pic_page;
        _500px.api('/photos', { feature: 'popular', page: pg + 1, image_size: [2, 4] }, function(response) {
            console.log(response.data);
            console.log(pg);
            var rsp = response.data.photos;
            this.piclst.pic_page += 1;
            this.piclst.pic_array = this.piclst.pic_array.concat(rsp);
            this.store();
            callback(pos);
        }.bind(this));
    },

    update_index: function(pos) {
        this.piclst.pic_index = pos;
        this.store();
        return;
    },

    store: function() {
        localStorage.pics = JSON.stringify({
            pic_array: this.piclst.pic_array,
            pic_page: this.piclst.pic_page,
            pic_index: this.piclst.pic_index,
            pic_bookmark: Array.from(this.piclst.pic_bookmark)
        });
    }
};

var p_view = {
    init: function() {
        console.log("view init");
        for (var i = 9; i >= 0; i--) {
            $(".photo").append("<div class='photo_box'><div class='photo_info'></div><img/><span class='p_like'><span></div>");
        }

    },


    populate: function(pics, pic_bookmark) {
        console.log('populate');
        var photo_box = $(".photo").find('img');
        var photo_info = $(".photo").find(".photo_info");
        var bookmark = $(".photo").find(".p_like");
        for (var i = pics.length - 1; i >= 0; i--) {
            $(photo_box[i]).attr({ 'src': pics[i].image_url[0] });
            $(photo_info[i]).html(pics[i].name);
            if (pic_bookmark.has(pics[i].image_url[0])) {
                $(bookmark[i]).attr({
                    status: "unlike",
                    class: "p_like unlike fa fa-thumbs-up w3-large"
                });
            } else {
                $(bookmark[i]).attr({
                    status: "like",
                    class: "p_like like fa fa-thumbs-o-up w3-large"
                });
            }
        }
    },

    pop_bookmark: function(pics, pic_bookmark) {
        var frame = $(".bookmark_pics");
        frame.empty();
        for (var i = pics.length - 1; i >= 0; i--) {
            var src = pics[i].image_url[0];
            if (pic_bookmark.has(src)) {
                var t = $("<div class='photo_box'><div class='photo_info'></div><img/><span class='p_like'><span></div>");
                // t.hide();
                t.find(".photo_info").html(pics[i].name);
                t.find("img").attr({ src: src })
                t.find(".p_like").attr({
                    status: "unlike",
                    class: "p_like unlike fa fa-thumbs-up w3-large"
                }).click(p_controller.thumb);
                frame.append(t);
                // t.show();
            }
        }
    },

    change_scn: function(piclst, pic_bookmark) {
        $(".photo").hide("fade", 400, function() {
            p_view.populate(piclst, pic_bookmark);
            $(".photo").show("fade", 400);
        });
    }
};

var p_controller = {
    init: function() {
        p_view.init();
        p_model.init(p_controller.init_ready.bind(this));
        $(".change_page").click(function() {
            console.log($(this).attr("dir"));
            p_controller.change_page($(this).attr("dir"));

        });
    },

    init_ready: function(pos) {
        console.log("ready");
        var lst = p_model.get_pics(pos, p_controller.init_ready);
        var pic_bookmark = p_model.piclst.pic_bookmark;
        p_view.populate(lst, pic_bookmark);

        $(".p_like").click(this.thumb);
        this.load_bookmark_page();
    },

    thumb: function() {
        var status = $(this).attr("status");
        var pic_url = $(this).prev().attr("src");
        switch (status) {
            case "like":
                $(this).attr({
                    status: "unlike",
                    class: "p_like unlike fa fa-thumbs-up w3-large"
                });
                p_model.piclst.pic_bookmark.add(pic_url);
                break;
            default:
                $(this).attr({
                    status: "like",
                    class: "p_like like fa fa-thumbs-o-up w3-large"
                });
                p_model.piclst.pic_bookmark.delete(pic_url);
        }

        p_model.store();
    },
    change_page: function(dir) {
        switch (dir) {
            case "next":
                var pos = p_model.piclst.pic_index + 10;
                break;
            default:
                var pos = p_model.piclst.pic_index - 10;
        }
        var piclst = p_model.get_pics(pos, p_controller.load_ready);
        if (piclst != undefined) {
            this.load_ready(pos);
        }
    },

    load_ready: function(pos) {
        if (pos < 0) {
            return;
        }
        var lst = p_model.get_pics(pos, p_controller.load_ready);
        var pic_bookmark = p_model.piclst.pic_bookmark;
        p_model.update_index(pos);
        p_view.change_scn(lst, pic_bookmark);
    },

    load_bookmark_page: function() {
        if (p_model.piclst == undefined) {
            p_controller.init();
        } else {
            var pics = p_model.piclst.pic_array;
            var bookmark = p_model.piclst.pic_bookmark;
            p_view.pop_bookmark(pics, bookmark);
        }
    }
};
p_controller.init();
