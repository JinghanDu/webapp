function showMarkedVideo() {
	console.log("in bookmark_videos");
	$(".bookmark_videos").empty();
	var marks = JSON.parse(localStorage.getItem('marks'));
	if(marks == null) return;
	var items = JSON.parse(localStorage.getItem('videos'));
	var len = 0;
	for(var i = 0; i < marks.length; i++) {
		if(marks[i] == "yes") {
			$(".bookmark_videos").append('<div class="bv"><iframe style="display:inline" class="favV" width="480" height="270" frameborder="0" allowfullscreen ></iframe> <i class="fa fa-thumbs-up" style="font-size:30px;position:relative;left:250px"></i></div>');
			// document.getElementsByClassName("fa fa-thumbs-up")[len].setAttribute("sid", i);
			$(".bookmark_videos:last").find('i').attr({sid:i});
			var frames = document.getElementsByClassName("favV");
			frames[len].src="//www.youtube.com/embed/" + items[i].id;			
 		     len++;
		}
	}
	$(".bookmark_videos").find('i').click(mark2);
}
function mark2() {
	console.log($(this).attr("sid"));
	var temp = $(this).attr("sid");
	var  id = parseInt(temp);
	var marks = JSON.parse(localStorage.getItem('marks'));
	if(marks[id] == "yes") {
		marks[id] = "no";
		$(this).attr({class:"fa fa-thumbs-o-up"});
	}else {
		marks[id] = "yes";
		$(this).attr({class:"fa fa-thumbs-up"});
	}
	localStorage.setItem('marks', JSON.stringify(marks));
	
}
function loadVideo() {
	showMarkedVideo();
}