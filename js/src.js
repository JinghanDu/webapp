var controller = {
    init: function() {
        console.log("run");
        $(".index_page").find('.inner').click(function() {
            console.log($(this).text());
            var scn1 = $(this).parent();
            var direction;
            switch ($(this).attr("target")) {
                case "video":
                    direction = "left";
                    video_controller.init();
                    break;
                case "photo":
                    direction = "right";
                    if (!p_model.piclst) { p_controller.init(); }
                    break;
                case "bookmark_pics":
                    direction = "up";
                    p_controller.load_bookmark_page();
                    showMarkedVideo();
            }
            var scn2 = $("." + $(this).attr("target")).parent();
            view.swtch(scn1, scn2, direction);
        });

        $(".back").click(function() {
            view.BackToIndex($(this).parents().eq(1), $(".index_page"));
        });
        $(".video_back").click(function() {
            view.BackToIndex($(this).parent(), $(".index_page"));
        });

        $(".index_page").tooltip({
            track: true,
            items: ".inner[title]",
        });

        $(".inner").hover(
            function() {
                $(this).find('i').animate({
                    "color": "#00BFFF",
                    "opacity": 1
                }, "200");
            },

            function() {
                $(this).find('i').animate({
                    "color": "#708090",
                    "opacity": 0.85
                }, "200");
            });
    }
};

var view = {
    swtch: function(scn1, scn2, dir) {
        // scn1 and scn2 are jquery object scn1 out scn2 in
        console.log(dir);
        scn2.show("fade", "slow");
        scn1.hide("slide", { direction: dir }, "slow");
        
    },

    BackToIndex: function(scn1, scn2) {
        scn1.hide("fade", "slow", function() { scn2.show("fade", "slow"); });
    }
}

controller.init();
