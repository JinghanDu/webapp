video_model = {
  apiKey: "AIzaSyCIxT4R3BFlGaK0zRiMwXEbOmquHa_H4ZM",
  blockNum: 6,
  retrieveNum: 12,
  sIndex: 0,

  // maxIndex: 0,
  // nextPage: ""
};
function loadMore() {
	console.log("key:  " + video_model.apiKey);
		gapi.client.init({
		 	'apikey': video_model.apiKey,
		}).then(function() {

		 	console.log("requesting  " );
			 	return gapi.client.request({
			 		    'path': 'youtube/v3/videos',
						'method': 'GET',						
						'params': {
							'part': 'snippet',
						    'type': 'video',
						    'maxResults': video_model.retrieveNum,
						    'chart':'mostPopular',
						    'key': video_model.apiKey,
						    'pageToken': localStorage.getItem('nextPage')
						    
						}
				})
		}).then(function(response) {
		 	console.log(response.result);
		 	var results = response.result;

		 	updateStorage(results); 
		 	video_view.fillThumbnails(video_model.sIndex);			
 			video_view.showDetails();       
		}), function(reason) {
		    console.log('Error: ' + reason.result.error.message);
		};

};
function updateStorage(results) {
	var oldList = localStorage.getItem('videos');
	var newList;
	var newmark = [];
	for(var i = 0; i < video_model.retrieveNum; i++) {
		newmark[i] = "no";
	}
	if(oldList == null) {
		newLsit = results.items;
	}else {
		 var oldmark = localStorage.getItem('marks');
		 newLsit = JSON.parse(oldList).concat(results.items);
		 newmark = JSON.parse(oldmark).concat(newmark);
	}
	
	localStorage.setItem('videos', JSON.stringify(newLsit));
	localStorage.setItem('marks', JSON.stringify(newmark));
	localStorage.setItem('nextPage', results.nextPageToken);
	var max = parseInt(localStorage.getItem('maxIndex')) + video_model.retrieveNum;
	localStorage.setItem('maxIndex', max.toString());
}
function start() {
	if(localStorage.getItem('maxIndex') == null) {
		localStorage.setItem('maxIndex', '0');
		localStorage.setItem('nextPage', "");
		// localStorage.setItem('videos', '');
	}
	if(video_model.sIndex >= parseInt(localStorage.getItem('maxIndex'))) {
		console.log("gapi.load");
		gapi.load('client', loadMore);
	}else {
		console.log("don't need to request");
		//console.log(localStorage.getItem('videos'));
		video_view.fillThumbnails(video_model.sIndex);
		video_view.showDetails();
	} 
	
}
function getNext() {
	video_model.sIndex++;
	if(video_model.sIndex >= parseInt(localStorage.getItem('maxIndex'))) {
		gapi.load('client', loadMore);
	}else if(video_model.sIndex % video_model.blockNum == 0) {
		video_view.fillThumbnails(video_model.sIndex);
		video_view.showDetails();
	}else {
		video_view.showDetails();
	}
}
function getPrev() {
	console.log(video_model.sIndex);
	if(video_model.sIndex == 0) {
		return;
	}
	video_model.sIndex--;
	
	if(video_model.sIndex % video_model.blockNum == video_model.blockNum - 1) {
		video_view.fillThumbnails(video_model.sIndex - video_model.blockNum + 1);
	}
	video_view.showDetails();
}
function mark() {
	var temp = $(this).parent().attr("c6id");
	var  id = parseInt(temp);
	var marks = JSON.parse(localStorage.getItem('marks'));
	if(marks[id] == "yes") {
		marks[id] = "no";
		// document.getElementById("fav").className = "fa fa-thumbs-o-up";
		$(this).attr({class: "fa fa-thumbs-o-up"});
	}else {
		marks[id] = "yes";
		//document.getElementById("fav").className = "fa fa-thumbs-up";
		$(this).attr({class: "fa fa-thumbs-up"});
	}
	localStorage.setItem('marks', JSON.stringify(marks));
	console.log(id + ": " + marks[id]);
}
video_controller = {
	init: function() {
		 
		 video_view.drawBlocks();
		 
		 document.getElementById("vnext").addEventListener("click", getNext);
		 document.getElementById("vprev").addEventListener("click", getPrev);
		 start();
		 //gapi.load('client', start);
	 }

};
video_view = {
	drawBlocks: function() {
		$(".c-3").append('<div class="c-5" style="display:inline-block"><iframe id="vresults" width="640" height="360" frameborder="0" allowfullscreen style="display:inline"></iframe></div>');
		$(".c-3").append('<div class="c-6" style="display:inline-block"><h2 id="title"></h2><h3 id="info"></h3><div id="description"></div><i id="fav" style="font-size:30px"></i></div>');
		for(var i = 0; i < video_model.blockNum; i++) { 				
 			 	$(".c-4").append('<img class="thumbnail" style="display:inline-block"/>');
 		}
 		var fav = document.getElementById('fav');
 		fav.addEventListener("click", mark);
	},
	fillThumbnails: function(start) {
		var items = JSON.parse(localStorage.getItem('videos'));
		var thumbs = document.getElementsByClassName("thumbnail"); 
		for(var i = 0; i < video_model.blockNum; i++) {
				var idx = i + start;
				thumbs[i].setAttribute("id", idx.toString());
				thumbs[i].src = items[idx].snippet.thumbnails.default.url;
				
 			}
	},
	showDetails: function() {
		var titles = document.getElementById("title");
 		var frames = document.getElementById("vresults");
 		var info = document.getElementById("info");
		var des = document.getElementById("description");
		var marks = JSON.parse(localStorage.getItem('marks'));
		var items = JSON.parse(localStorage.getItem('videos'));
		
				
 		if(marks[video_model.sIndex] == "no") {
 			document.getElementById("fav").className = "fa fa-thumbs-o-up";
 		}else {
 			document.getElementById("fav").className = "fa fa-thumbs-up";
 		}
 		titles.innerHTML = items[video_model.sIndex].snippet.title;
 		info.innerHTML = "from " + items[video_model.sIndex].snippet.channelTitle +"\n" + 
 						" on " +  items[video_model.sIndex].snippet.publishedAt.substring(0, 10);

 		frames.src="//www.youtube.com/embed/" + items[video_model.sIndex].id;
 		des.innerHTML = items[video_model.sIndex].snippet.description.substring(0,300) + "...";
 		//document.getElementsByClassName("c-6")[0].setAttribute("id", video_model.sIndex);
 		document.getElementsByClassName("c-6")[0].setAttribute("c6id", video_model.sIndex);
	}

	
};




